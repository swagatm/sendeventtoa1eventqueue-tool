import com.appnomic.appsone.common.protbuf.A1EventProtos;
import com.appnomic.appsone.common.protbuf.NotificationProtos;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import java.io.FileReader;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.*;
import java.util.concurrent.TimeoutException;

public class MainClass {
    private final static String QUEUE_NAME = "a1-event-messages";
    private static final AMQP.BasicProperties persistent = new AMQP.BasicProperties.Builder().deliveryMode(2).build();
    public static void main(String[] args) throws IOException, TimeoutException, KeyStoreException, CertificateException, NoSuchAlgorithmException, UnrecoverableKeyException, KeyManagementException, InterruptedException {
        String configPath = args[0];
        JSONObject config = readConfig(configPath);
        ConnectionFactory factory = new ConnectionFactory();
        factory.useSslProtocol("TLSv1.2");
        factory.setHost((String) ((JSONObject) config.get("connectionData")).get("host"));
        factory.setPort(Integer.parseInt((String) ((JSONObject) config.get("connectionData")).get("port")));
        System.out.println((String) ((JSONObject) config.get("connectionData")).get("host"));
        System.out.println(Integer.parseInt((String) ((JSONObject) config.get("connectionData")).get("port")));
        factory.setVirtualHost("/");
        factory.setAutomaticRecoveryEnabled(true);
        factory.setConnectionTimeout(5000);

        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare(QUEUE_NAME, true, false, false, null);

        A1EventProtos.A1Event event = getEvent((JSONObject)config.get("payloadData"),(JSONObject)config.get("contactDetails"));
        System.out.println(event);
        String corrId = UUID.randomUUID().toString();
        AMQP.BasicProperties persistent = new AMQP.BasicProperties.Builder().deliveryMode(2).correlationId(corrId).build();

        try{
            channel.basicPublish("", QUEUE_NAME, persistent, event.toByteArray());
        }
        catch(Throwable t){
            System.out.println(t);
        }
        channel.close();
        connection.close();
    }

    private static JSONObject readConfig(String configPath) {
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = null;
        try {
            FileReader fr = new FileReader(configPath);
            jsonObject = (JSONObject) jsonParser.parse(fr);
            fr.close();
        } catch (org.json.simple.parser.ParseException | IOException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private static A1EventProtos.A1Event getEvent(JSONObject payloadData, JSONObject contactDetails) {
        List<NotificationProtos.UserDetails> userDetails = new ArrayList<>();
        userDetails.add(NotificationProtos.UserDetails.newBuilder()
                .setContactNumber((String) contactDetails.get("mobileNumber"))
                .setEmailAddress((String) contactDetails.get("emailId"))
                .build());
        Map<String, String> placeholdersMap = new HashMap();
        for (Object name : payloadData.keySet()){
            placeholdersMap.put(name.toString(), (String) payloadData.get(name));
        }
        NotificationProtos.Notification notification = NotificationProtos.Notification
                .newBuilder()
                .setAccountId(placeholdersMap.get("AccountIdentifier"))
                .setSourceType(placeholdersMap.get("Signal_Type"))
                .addAllUsersDetailsList(userDetails)
                .putAllPlaceHolders(placeholdersMap)
                .build();
        A1EventProtos.A1Event event = A1EventProtos.A1Event.newBuilder()
                .setEventType("NOTIFICATION_OUTPUT")
                .setEventData(notification.toByteString())
                .build();
        return event;
    }
}