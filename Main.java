import com.appnomic.appsone.common.protbuf.A1EventProtos;
import com.appnomic.appsone.common.protbuf.NotificationProtos;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.boon.core.Sys;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.concurrent.TimeoutException;
import java.util.UUID;
public class Main {
    private final static String QUEUE_NAME = "a1-event-messages";
    private static final AMQP.BasicProperties persistent = new AMQP.BasicProperties.Builder().deliveryMode(2).build();
//    public static void main(String[] args) throws IOException, TimeoutException, KeyStoreException, CertificateException, NoSuchAlgorithmException, UnrecoverableKeyException, KeyManagementException, InterruptedException {
//        String configPath = args[0];
//        JSONObject config = readConfig(configPath);
//        ConnectionFactory factory = new ConnectionFactory();
//        //factory.setHost("44.230.236.75");
//        factory.useSslProtocol("TLSv1.2");
////        factory.useSslProtocol();
//        factory.setHost((String) config.get("host"));
//        factory.setPort(Integer.parseInt((String) config.get("port")));
//        System.out.println((String) config.get("host"));
//        System.out.println(Integer.parseInt((String) config.get("port")));
//
////        factory.setUsername("guest");
////        factory.setPassword("guest");
//        factory.setVirtualHost("/");
//        factory.setAutomaticRecoveryEnabled(true);
//        factory.setConnectionTimeout(5000);
//
//        Connection connection = factory.newConnection();
//        Channel channel = connection.createChannel();
//        channel.queueDeclare(QUEUE_NAME, true, false, false, null);
//        //send Anomaly event
//        A1EventProtos.A1Event event = getEvent((String) config.get("Source_type"), (String) config.get("Target_Service"),config);
//        System.out.println(event);
//        String corrId = UUID.randomUUID().toString();
//        AMQP.BasicProperties persistent = new AMQP.BasicProperties.Builder().deliveryMode(2).correlationId(corrId).build();
//
//        try{
//            channel.basicPublish("", QUEUE_NAME, persistent, event.toByteArray());
//        }
//        catch(Throwable t){
//            System.out.println(t);
//        }
//        channel.close();
//        connection.close();
//
//        //send Problem event
////        event = getEvent("Problem", "TestService_1", config);
////        channel.basicPublish("", QUEUE_NAME, null, event.toByteArray());
////        Thread.sleep(1000*60);
////        //should be suppressed
////        A1EventProtos.A1Event eventToSupress = getEvent("Early Warning", "TestService_1", config);
////        channel.basicPublish("", QUEUE_NAME, null, eventToSupress.toByteArray());
////        Thread.sleep(1000*60*3);
////        //should use default suppression interval from config and suppress the ticket
////        A1EventProtos.A1Event eventDefaultSuppressionInterval = getEvent("Early Warning", "Default_Service", config);
////        channel.basicPublish("", QUEUE_NAME, null, eventDefaultSuppressionInterval.toByteArray());
//
////        System.exit(-1);
//    }

    private static JSONObject readConfig(String configPath) {
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = null;
        try {
            FileReader fr = new FileReader(configPath);
            jsonObject = (JSONObject) jsonParser.parse(fr);
            fr.close();
        } catch (org.json.simple.parser.ParseException | IOException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private static A1EventProtos.A1Event getEvent(String sourceType, String serviceName, JSONObject config) {
        List<NotificationProtos.UserDetails> userDetails = new ArrayList<>();
        userDetails.add(NotificationProtos.UserDetails.newBuilder()
                .setContactNumber("123456789")
                .setEmailAddress("appsone_qa@appnomic.com")
                .build());
        Map<String, String> placeholdersMap = new HashMap();
        placeholdersMap.put("AccountIdentifier", "1234567890");
        placeholdersMap.put("Signal_ID", (String) config.get("EarlyWarning_Signal_ID"));
        placeholdersMap.put("Signal_Status", (String) config.get("Signal_Status"));
        placeholdersMap.put("Signal_Type", sourceType);
        placeholdersMap.put("Signal_Description", "Test App");
        placeholdersMap.put("Severity", "Severe");
        placeholdersMap.put("StartTime", System.currentTimeMillis() + "");
        placeholdersMap.put("EndTime", System.currentTimeMillis() + "");
        placeholdersMap.put("Username", "TestUser");
        placeholdersMap.put("Organization_Name", "Test Org");
        placeholdersMap.put("App_Names", "Test App");
        placeholdersMap.put("RootCause_ServiceNames", serviceName);
        placeholdersMap.put("Affected_ServiceNames", serviceName);
        placeholdersMap.put("Affected_ApplicationNames", "Test app");
        placeholdersMap.put("Total_Events", "4");
        placeholdersMap.put("Latest_Events", "P-2-400-8-26520010");
        placeholdersMap.put("Latest_Event_Time", System.currentTimeMillis() + "");
        placeholdersMap.put("Latest_Events_Detected", "<ul><li>Service" +
                " Name: "+serviceName+"</li><li>Request Name: osb-upi</li><li>Host " +
                "address:192.168.13.131</li><li>KPI name: "+ config.get("KPI_Name") +"</li><li>KPI attribute: " +
                "ALL</li><li>Value: 37320.0</li><li>Unit: Percentage</li><li>Operation: " +
                "not between</li><li>Lower threshold: 557.91</li><li>Upper Threshold:" +
                " 1033.81</li></ul>");
        placeholdersMap.put("A1Port", "1234");
        placeholdersMap.put("A1Protocol", "https");
        placeholdersMap.put("A1HostAddress", "10.0.245.22");
        if (sourceType.equalsIgnoreCase("PROBLEM")) {
            placeholdersMap.put("EntryPoint_ServiceName", serviceName);
            placeholdersMap.put("Related_Signals", (String) config.get("Problem_Signal_ID"));
        }

        NotificationProtos.Notification notification = NotificationProtos.Notification
                .newBuilder()
                .setAccountId("1")
                .setSourceType(sourceType)
                .addAllUsersDetailsList(userDetails)
                .putAllPlaceHolders(placeholdersMap)
                .build();
        A1EventProtos.A1Event event = A1EventProtos.A1Event.newBuilder()
                .setEventType("NOTIFICATION_OUTPUT")
                .setEventData(notification.toByteString())
                .build();
        return event;
    }
}